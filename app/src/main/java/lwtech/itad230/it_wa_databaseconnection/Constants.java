package lwtech.itad230.it_wa_databaseconnection;

public class Constants {
    //private static final String ROOT_URL = "http://10.0.0.154:8181/IT_project/web_services/";
    private static final String ROOT_URL = "http://wardrobe-assistant.icoolshow.net/IT_project/web_services/";
    public static final String URL_REGISTER = ROOT_URL+"register_user.php";
    public static final String URL_LOGIN = ROOT_URL+"user_login.php";
    public static final String URL_ADDITEM = ROOT_URL+"add_Item.php";
    public static final String URL_BROWSEWARDROBE = ROOT_URL+"filter_design.php";
    public static final String URL_LAST_VIEWED = ROOT_URL+"last_viewed.php";
}

